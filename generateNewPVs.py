""" Script to generate the new PV names and associated them to the right
terminals. Used for PRL TCBs"""

from epics import caget
import pandas as pd

CSVTCB="TCBprefixes.csv"
TCBBOXES=17

items = pd.read_csv(CSVTCB)

prefixes = items.to_numpy().T[0]
el2502ch1 = list("trnjfb".upper())
el2502ch2 = list("sqmiea".upper())
el3202= list("polkhgdc".upper())

# the first slave (1, 17, ...) is always a EK1501, that will not be mapped
# the middle slave (8, 24, ...) is always a EL9410, and will not be mapped

tcb_num = 0

print('file "$(E3_CMD_TOP)/db/Power.template" ')
print('{ \n pattern {P, R, PTCB, CH, SLNUM, BONUM}')

invalids = 0
for tcb_num in range(TCBBOXES): # n tcbs
    tcb_id = caget("PRL:ec0-s" + str(tcb_num * 16 + 1) + "-EK1501-0010-ID")
    if tcb_id is None or tcb_id > 20:
        invalids += 1
        continue

    p = prefixes[int(tcb_id)]
    for sl in range(6): # EL2502 from slave 2 to 7
        print("{" + p + ", :, $(IOC), " + el2502ch1[sl] + "1, " + str(sl+2+(tcb_num*16))+ ", 1}")
        print("{" + p + ", :, $(IOC), " + el2502ch2[sl] + "2, " + str(sl+2+(tcb_num*16))+ ", 2}")

print('}')
print("Invalids ", invalids)

print('file "$(E3_CMD_TOP)/db/Temperature.template"')
print('{ \n pattern {P, R, PTCB, CH, SLNUM, AINUM}')

invalids = 0

for tcb_num in range(TCBBOXES): # n tcbs
    tcb_id = caget("PRL:ec0-s" + str(tcb_num * 16 + 1) + "-EK1501-0010-ID")
    if tcb_id is None or tcb_id > 20:
        invalids += 1
        continue

    p = prefixes[int(tcb_id)]

    for sl in range(8): #EL3202-0010 from slave 9 to 16
        print("{" + p + ", :, $(IOC), " + el3202[sl] + "1, " + str(sl+9+(tcb_num*16))+ ", 1}")
        print("{" + p + ", :, $(IOC), " + el3202[sl] + "2, " + str(sl+9+(tcb_num*16))+ ", 2}")
print('}')


print("Invalids ", invalids)
