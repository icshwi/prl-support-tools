""" Script to generate the new PV names and associated them to the right
terminals. Used for PRL TCBs"""

from epics import caget
import pandas as pd

CSVTCB="TCBprefixes.csv"
TCBBOXES=17

items = pd.read_csv(CSVTCB)

prefixes = items.to_numpy().T[0]
#file "$(E3_CMD_TOP)/db/Power.template"
#{
#         pattern {P, R}
#         {HEBT-020RFC:RFS-PRLTCB-019, :}
#
#        }
#
#
#file "$(E3_CMD_TOP)/db/Temperature.template"
#{
#         pattern {P, R}
#         {HEBT-020RFC:RFS-PRLTCB-019, :}
#
#        }

start="""file "$(E3_CMD_TOP)/db/Power.template"
{
\tpattern {P, R}
"""

tcb_num = 1
for p in prefixes:
    with open("TCB-Power-%d.substitutions" % (tcb_num), "w") as f:
        content = start + "\t{%s, :}" %(p) + "\n}"
        f.write(content)
    tcb_num += 1

start="""file "$(E3_CMD_TOP)/db/Temperature.template"
{
\tpattern {P, R}
"""

tcb_num = 1
for p in prefixes:
    with open("TCB-Temp-%d.substitutions" % (tcb_num), "w") as f:
        content = start + "\t{%s, :}" %(p) + "\n}"
        f.write(content)
    tcb_num += 1


