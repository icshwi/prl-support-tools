import re

inp_file="input.txt"
out_file="output_%d.txt"

# how many files to be generated
files=16

def generate_new_file(inp_file, out_file, new_start):
    """
    Generate a new file substituting the SLAVE_ID values for
    a (+ new_start) value
    """
    inp = open(inp_file)
    out = open(out_file, "w+")

    i = 0
    expr = "=%d," % i
    sub_expr = "=%d," % (i + new_start)
    p = re.compile(expr)

    for l in inp.readlines():
        new_line = l
        # finds the expression on this line
        if len(p.findall(l)):
            new_line = p.sub(sub_expr, l)
            i += 1
            expr = "=%d," % i
            sub_expr = "=%d," % (i + new_start)
            p = re.compile(expr)

        out.write(new_line)


for i in range(files):
    generate_new_file(inp_file, out_file % i, (i+1)*16 )
