# Script to check if all the new PVs are created correctly
# and using the right INP/OUT

from epics import caget
import pandas as pd

CSVTCB="TCBprefixes.csv"
TCBBOXES=17

items = pd.read_csv(CSVTCB)

prefixes = items.to_numpy().T[0]
el2502ch1 = list("trnjfb".upper())
el2502ch2 = list("sqmiea".upper())
el3202= list("polkhgdc".upper())

# the first slave (1, 17, ...) is always a EK1501, that will not be mapped
# the middle slave (8, 24, ...) is always a EL9410, and will not be mapped

for tcb_num in range(TCBBOXES): # n tcbs
    pv_tcbid = "PRL:ec0-s" + str(tcb_num * 16 + 1) + "-EK1501-0010-ID"
#    print("PV TCB ID: ", pv_tcbid)
    tcb_id = caget(pv_tcbid)
#    print("TCB ID: ", tcb_id)
    if tcb_id is None or tcb_id > 20:
        continue

    p = prefixes[int(tcb_id-1)]
    for sl in range(6): # EL2502 from slave 2 to 7
        pv = p + ":PowerCh%s%s-RB" % (el2502ch1[sl], str(1))
        exist = caget(pv)
        if exist is None:
            print("Error on PV : ", pv)


        pv = p + ":PowerCh%s%s-RB" % (el2502ch1[sl], str(1))
        pvlink = (caget(pv + ".INP")).split(" ")[0]
        exit = caget(pvlink)
        if exist is None:
            print("Error on PV : ", pv, pvlink)


        pv = p + ":PowerCh%s%s-RB" % (el2502ch2[sl], str(2))
        exist = caget(pv)
        if exist is None:
            print("Error on PV : ", pv)


        pv = p + ":PowerCh%s%s-RB" % (el2502ch2[sl], str(2))
        pvlink = (caget(pv + ".INP")).split(" ")[0]
        exit = caget(pvlink)
        if exist is None:
            print("Error on PV : ", pv, pvlink)


        pv = p + ":PowerCh%s%s-S" % (el2502ch1[sl], str(1))
        exist = caget(pv)
        if exist is None:
            print("Error on PV : ", pv)


        pv = p + ":PowerCh%s%s-S" % (el2502ch1[sl], str(1))
        pvlink = (caget(pv + ".OUT")).split(" ")[0]
        exit = caget(pvlink)
        if exist is None:
            print("Error on PV : ", pv, pvlink)


        pv = p + ":PowerCh%s%s-S" % (el2502ch2[sl], str(2))
        exist = caget(pv)
        if exist is None:
            print("Error on PV : ", pv)


        pv = p + ":PowerCh%s%s-S" % (el2502ch2[sl], str(2))
        pvlink = (caget(pv + ".OUT")).split(" ")[0]
        exit = caget(pvlink)
        if exist is None:
            print("Error on PV : ", pv, pvlink)

        print(".")

for tcb_num in range(TCBBOXES): # n tcbs
    pv_tcbid = "PRL:ec0-s" + str(tcb_num * 16 + 1) + "-EK1501-0010-ID"
#    print("PV TCB ID: ", pv_tcbid)
    tcb_id = caget(pv_tcbid)
#    print("TCB ID: ", tcb_id)
    if tcb_id is None or tcb_id > 20:
        continue

    p = prefixes[int(tcb_id-1)]
    for sl in range(8): # EL2502 from slave 9 to 16
        pv = p + ":TempCh%s%s-R" % (el3202[sl], str(1))
        exist = caget(pv)
        if exist is None:
            print("Error on PV : ", pv)

        pv = p + ":TempCh%s%s-R" % (el3202[sl], str(1))
        pvlink = (caget(pv + ".INP")).split(" ")[0]
        exit = caget(pvlink)
        if exist is None:
            print("Error on PV : ", pv, pvlink)


        pv = p + ":TempCh%s%s-R" % (el3202[sl], str(2))
        exist = caget(pv)
        if exist is None:
            print("Error on PV : ", pv)

        pv = p + ":TempCh%s%s-R" % (el3202[sl], str(2))
        pvlink = (caget(pv + ".INP")).split(" ")[0]
        exit = caget(pvlink)
        if exist is None:
            print("Error on PV : ", pv, pvlink)


        print(".")
